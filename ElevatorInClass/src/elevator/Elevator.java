package elevator;

/**
 * Elevator class to simulate the operation of an elevator.  Details
 * associated with opening and closing of the elevator doors, entry and
 * exit of people to and from the elevator, the number of people in, 
 * entering or leaving the elevator, and the timing of the movement of
 * the elevator are all "abstracted" out of the problem, encompassing 
 * all of these actions into a single "move()" operation.
 * 
 * @author Daniel Plante
 *
 */
public class Elevator implements ElevatorOperations
{
	public static final int NO_VALUE = -1;
	private int myNumberOfFloors;
	private int myDirection;
	private int myNextDirection;
	private int mySequenceNumber;
	private int myPresentFloor;
	
	private int[] myUpButtonOuter;
	private int[] myDownButtonOuter;
	private boolean[] myInnerButtons;
	
	/**
	 * Default constructor setting the number of floors for
	 * the elevator to five (5)
	 */
	public Elevator()
	{
		this(5);
	}
	
	/**
	 * Constructor setting the number of floors
	 * 
	 * @param numFloors total number of floors
	 */
	public Elevator(int numFloors)
	{
	}
	

	/**
	 * Try to push the "up" button on a floor, and fail gracefully if
	 * not a valid choice or already pushed.
	 * 
	 * @param floor the floor the up button is pushed on
	 * 
	 * @return true if the button was pushed, false if not
	 */
	public boolean pushUp(int floor)
	{
		return true;
	}
	
	/**
	 * Try to push the "down" button on a floor, and fail gracefully if
	 * not a valid choice or already pushed.
	 * 
	 * @param floor the floor the down button is pushed on
	 * 
	 * @return true if the button was pushed, false if not
	 */
	public boolean pushDown(int floor)
	{
		return true;
	}
	
	/**
	 * If a valid thing to do, push a button inside the elevator
	 */
	public boolean pushIn(int floor)
	{
		return true;
	}
	
	/**
	 * Try to actually move the elevator based on all present settings.  
	 * 
	 * @return the floor the elevator is on after complete
	 */
	public int move()
	{		
		return 0;
	}
	
	/**
	 * Accessor returning the current floor
	 * 
	 * @return floor presently on
	 */
	public int getFloor()
	{
		return 0;
	}
	
	/**
	 * Accessor returning the current direction
	 * 
	 * @return direction presently set
	 */
	public int getDirection()
	{
		return 0;
	}
}