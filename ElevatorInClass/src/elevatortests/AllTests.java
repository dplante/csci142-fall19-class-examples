package elevatortests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestElevator_DP.class, TestElevator_Basics.class, TestElevator_Students.class})
public class AllTests {

}
