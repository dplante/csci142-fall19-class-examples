package figurestesting;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import figures.Square;

public class TestSquare 
{
	private Square mySquare;

	@Before
	public void setUp() throws Exception 
	{
		mySquare = new Square();
	}

	@Test
	public void testDefaultSquareArea() {
		float area = mySquare.getArea();
		assertEquals("Side length should be 1.0", 1.0f, mySquare.getSideLength(), 0.00001);
		assertEquals("Area should be 1.0", 1.0f, area, 0.0000001);
	}
	
	@Test
	public void testSquareChangeSide() {
		mySquare.setSideLength(5.0f);
		float area = mySquare.getArea();
		assertEquals("Side length should be 5.0", 5.0f, mySquare.getSideLength(), 0.00001);
		assertEquals("Area should be 25.0", 25.0f, area, 0.0000001);
	}

}
