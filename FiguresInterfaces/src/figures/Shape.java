package figures;

/**
 * Interface Shape is 100% abstract and only defines the
 * method declarations.
 * 
 * Interfaces are a form of "design by contract", where the
 * interface IS the contract.
 * 
 * @author dplante
 *
 */
public interface Shape 
{
	/*
	 * You can use the keyword "abstract" to define each
	 * method but typically don't do that.
	 */
	public abstract String toString();
	public float getArea();
	public String getType();
	public void setType(String type);
}
