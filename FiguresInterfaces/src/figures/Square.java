package figures;

/**
 * Square class the is very simple and only works
 * with the side length and area (and type)
 * 
 * @author dplante
 *
 */
public class Square implements Shape
{
	private float myArea;
	private String myType;
	
	/*
	 * Properties (or attributes or fields) of the
	 * Square class; note that you should NEVER comment
	 * like below!!  Self document as much as possible!
	 */
	private float mySideLength;  // this is the side length
	
	/**
	 * Default Square constructor
	 */
	public Square()
	{
		this(1.0f);
	}
	
	/**
	 * Square constructor
	 * 
	 * @param len side length of square
	 */
	public Square(float len)
	{
		myType = "Square";
		mySideLength = len;
		this.calculateArea(len);
	}
	
	/**
	 * Calculate the area of the square
	 * 
	 * @param len side length of square
	 */
	private void calculateArea(float len)
	{
		/*
		 * Don't use pow!  Just do it simply
		 */
		//myArea = (float)Math.pow(len, 2);
		myArea = len * len;
	}
	
	public String toString()
	{
		String str = myType + ":  length = " + mySideLength 
				   + ":  area = " + myArea;
		return str;
	}

	/*
	 * Accessors and Mutators
	 * (or Setters and Getters)
	 * 
	 * You don't have to comment your accessors and
	 * mutators when they only set and get.
	 */
	
	public float getSideLength() 
	{
		return mySideLength;
	}

	/*
	 * setSideLength() uses my coding standards, 
	 * but the rest of these don't; I won't require
	 * you to follow my coding standards for setters
	 * and getters
	 */
	/**
	 * Setter for side length with side effect that the
	 * area is also changed automatically
	 * 
	 * @param len side length
	 */
	public void setSideLength(float len) 
	{
		mySideLength = len;
		//this.calculateArea(len);
	}

	public float getArea() 
	{
		return myArea;
	}

	public String getType() 
	{
		return myType;
	}

	public void setType(String type) 
	{
		myType = type;
	}
}
