package battleship;

import static org.junit.Assert.assertEquals;

import java.awt.Point;
import java.util.Vector;

public class ShipBoard 
{
	public final int SIZE = 10;
	
	private PegType[][] myGrid;
	private PegType myLastMove;
	private Vector<Ship> myShips=new Vector<Ship>(5);
	public ShipBoard()
	{
		myGrid = new PegType[SIZE][SIZE];
		for(int i = 0; i < 10; i++)
		{
			for(int j = 0; j < 10; j++)
			{
				myGrid[i][j]=PegType.NONE;
			}
		}
		myLastMove = PegType.NONE;
	}
	public boolean placePeg(Point point)
	{
		//if(point.getX() >= 0 && point.getX() <= 9 && point.getY() >= 0 && point.getY() <= 9)
		if(isValidMove(point))
		{
			for(Ship i:myShips)
			{
				boolean isvertical;
				if(i.getFrontLocation().getX()==i.getBackLocation().getX())
				{
					isvertical=true;
				}
				else
				{
					isvertical=false;
				}
				int frontx=(int)i.getFrontLocation().getX();
				int fronty=(int)i.getFrontLocation().getY();
				int backx=(int)i.getBackLocation().getX();
				int backy=(int)i.getBackLocation().getY();
				//TEST WHICH SQUARES ARE OCCUPIED
				if(isvertical)
				{
					if(fronty<backy)
					{
						for(int y=fronty; y<=backy; y++)
						{
							if((point.getX()==frontx)&&(point.getY()==y))
							{
								myGrid[frontx][y]=PegType.RED;
								i.placePeg(new Point(frontx, y));
								myLastMove=PegType.RED;
								return true;
							}
						}
					}
					else
					{
						for(int y=backy; y<=fronty; y++)
						{
							if((point.getX()==frontx)&&(point.getY()==y))
							{
								myGrid[frontx][y]=PegType.RED;
								i.placePeg(new Point(frontx, y));
								myLastMove=PegType.RED;
								return true;
							}						
						}
					}
				}
				else
				{
					if(frontx<backx)
					{
						for(int x=frontx; x<=backx; x++)
						{
							if((point.getX()==x)&&(point.getY()==fronty))
							{
								myGrid[x][fronty]=PegType.RED;
								i.placePeg(new Point(x, fronty));
								myLastMove=PegType.RED;
								return true;
							}
						}
					}
					else
					{
						for(int x=backx; x<=frontx; x++)
						{
							if((point.getX()==x)&&(point.getY()==fronty))
							{
								myGrid[x][fronty]=PegType.RED;
								i.placePeg(new Point(x, fronty));
								myLastMove=PegType.RED;
								return true;
							}
						}
					}
				}
			}
			myGrid[(int)point.getX()][(int)point.getY()]=PegType.WHITE;
			myLastMove=PegType.WHITE;
			return true;
		}
		return false;
	}
	public boolean isValidMove(Point point)
	{
		if(point.getX() >= 0 && point.getX() <= 9 && point.getY() >= 0 && point.getY() <= 9 && myGrid[(int)point.getX()][(int)point.getY()]==PegType.NONE)
		{
			return true;
		}
		return false;
	}
	public boolean placeShips()
	{
		Ship[] tempships=new Ship[5];
		tempships[0]=new Ship(ShipType.CARRIER, 5);
		tempships[1]=new Ship(ShipType.BATTLESHIP, 4);
		tempships[2]=new Ship(ShipType.CRUISER, 3);
		tempships[3]=new Ship(ShipType.SUBMARINE, 3);
		tempships[4]=new Ship(ShipType.DESTROYER, 2);
		boolean placed=false;
		for(int x=0; x<5; x++)
		{
			while(!placed)
			{
				Point frnt=new Point((int)(Math.random()*10), (int)(Math.random()*10));
				Point bck=new Point((int)(Math.random()*10), (int)(Math.random()*10));
				placed=placeShip(tempships[x].getShipType(), frnt, bck);
			}
			placed=false;
		}
		return true;
	}
	public void resetBoard()
	{
		myGrid=new PegType[SIZE][SIZE];
		for(int i = 0; i < 10; i++)
		{
			for(int j = 0; j < 10; j++)
			{
				myGrid[i][j]=PegType.NONE;
			}
		}
		for(int x=0; x<myShips.size(); x++)
		{
			if(myShips.get(x)!=null)
			{
				myShips.get(x).resetShip();
			}
		}
	}
	public boolean placeShip(ShipType type, Point front, Point back)
	{
		int length=type.getLength();
		Point[] occupied=new Point[length];
		Ship ship=new Ship(type, length);
		if(!(ship.setFrontLocation(front)&&ship.setBackLocation(back)))
		{
			return false;
		}
		boolean isvertical;
		if(front.getX()==back.getX())
		{
			isvertical=true;
		}
		else
		{
			isvertical=false;
		}
		int frontx=(int)front.getX();
		int fronty=(int)front.getY();
		int backx=(int)back.getX();
		int backy=(int)back.getY();
		//TEST WHICH SQUARES ARE OCCUPIED
		if(isvertical)
		{
			if(fronty<backy)
			{
				for(int x=fronty; x<=backy; x++)
				{
					occupied[x-fronty]=new Point(frontx, x);
				}
			}
			else
			{
				for(int x=backy; x<=fronty; x++)
				{
					occupied[x-backy]=new Point(frontx, x);
				}
			}
		}
		else
		{
			if(frontx<backx)
			{
				for(int x=frontx; x<=backx; x++)
				{
					occupied[x-frontx]=new Point(x, fronty);
				}
			}
			else
			{
				for(int x=backx; x<=frontx; x++)
				{
					occupied[x-backx]=new Point(x, fronty);
				}
			}
		}
		for(Ship i:myShips)
		{
			int ilen=i.getLength();
			boolean iisvertical;
			int ifx=(int)i.getFrontLocation().getX();
			int ify=(int)i.getFrontLocation().getY();
			int ibx=(int)i.getBackLocation().getX();
			int iby=(int)i.getBackLocation().getY();
			if(ifx==ibx)
			{
				iisvertical=true;
			}
			else
			{
				iisvertical=false;
			}
			if(iisvertical)
			{
				if(ify<iby)
				{
					for(int b=ify; b<iby; b++)
					{
						for(Point p:occupied)
						{
							Point z=new Point(ifx, b);
							if((p.getX()==z.getX())&&(p.getY()==z.getY()))
							{
								return false;
							}
						}
					}
				}
				else
				{
					for(int b=iby; b<ify; b++)
					{
						for(Point p:occupied)
						{
							Point z=new Point(ifx, b);
							if((p.getX()==z.getX())&&(p.getY()==z.getY()))
							{
								return false;
							}
						}
					}
				}
			}
			else
			{
				if(ifx<ibx)
				{
					for(int b=ifx; b<ibx; b++)
					{
						for(Point p:occupied)
						{
							Point z=new Point(b, ify);
							if((p.getX()==z.getX())&&(p.getY()==z.getY()))
							{
								return false;
							}
						}
					}
				}
				else
				{
					for(int b=ibx; b<ifx; b++)
					{
						for(Point p:occupied)
						{
							Point z=new Point(b, ify);
							if((p.getX()==z.getX())&&(p.getY()==z.getY()))
							{
								return false;
							}
						}
					}
				}
			}
		}
		if(ship.setFrontLocation(front)&&ship.setBackLocation(back))
		{
			myShips.add(ship);
			return true;
		}
		return false;
	}
	public int numberShipsSunk()
	{
		int num=0;
		for(Ship i:myShips)
		{
			if(i.isSunk())
			{
				num++;
			}
		}
		return num;
	}
	public boolean allShipsSunk()
	{
		for(Ship i:myShips)
		{
			if(!i.isSunk())
			{
				return false;
			}
		}
		return true;
	}
	public PegType getLastMove()
	{
		return myLastMove;
	}
	public PegType[][] getGrid()
	{
		return myGrid;
	}
	public Vector<Ship> getShips()
	{
		return myShips;
	}
}
