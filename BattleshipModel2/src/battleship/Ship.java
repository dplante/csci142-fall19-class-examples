package battleship;

import java.awt.Point;
import java.util.Vector;

public class Ship
{
	private Point myFrontLocation;
	private Point myBackLocation;
	private int myLength;
	private Vector<Boolean> myHits;
	private boolean mySunk;
	private ShipType myShipType;
	public Ship(ShipType ship, int length)
	{
		myShipType=ship;
		myLength = length;
		myHits = new Vector<Boolean>();
		for(int i = 0; i < myLength; ++i)
		{
			myHits.add(false);
		}
		myFrontLocation = null;
		myBackLocation = null;
		mySunk = false;
	}
	public boolean placePeg(Point location)
	{
		Point front=this.getFrontLocation();
		Point back=this.getBackLocation();
		int frontx=(int) myFrontLocation.getX();
		int fronty=(int) myFrontLocation.getY();
		int backx=(int) myBackLocation.getX();
		int backy=(int) myBackLocation.getY();
		int shotx=(int) location.getX();
		int shoty=(int) location.getY();
		if(frontx==backx) //if the ship is vertical
		{
			if(((shoty>=fronty)&&(shoty<=backy))||((shoty<=fronty)&&(shoty>=backy)))//if the shot hit the ship
			{
				int x=Math.abs(shoty-fronty);
				if(myHits.get(x))
				{
					return false;
				}
				myHits.set(x, true);
			}
		}
		if(fronty==backy) //if the ship is horizontal
		{
			if(((shotx>=frontx)&&(shotx<=backx))||((shotx<=frontx)&&(shotx>=backx)))//if the shot hit the ship
			{
				int x=Math.abs(shotx-frontx);
				if(myHits.get(x))
				{
					return false;
				}
				myHits.set(x, true);
			}
		}
		int y=0;
		for(int i = 0; i < myLength; i++)
		{
			if(myHits.get(i))
			{
				y++;
			}
		}
		if(y==myLength)
		{
			mySunk=true;
		}
		return true;
	}
	public boolean isSunk()
	{
		return mySunk;
	}
	public boolean setFrontLocation(Point location)
	{
		if(location.getX() >= 0 && location.getX() <= 9 && location.getY() >= 0 && location.getY() <= 9)
		{
			myFrontLocation=location;
			return true;	
		}
		else
		{
			return false;
		}
	}
	public boolean setBackLocation(Point location)
	{
		Point front=this.getFrontLocation();
		if(front==null)
		{
			return false;
		}
		if((location.getX()!=front.getX())&&(location.getY()!=front.getY()))
		{
			return false;
		}
		if(location.getX()==front.getX()) 
		{
			if(Math.abs(location.getY()-front.getY())!=this.getLength()-1)
			{
				return false;
			}
		}
		if(location.getY()==front.getY()) 
		{
			if(Math.abs(location.getX()-front.getX())!=this.getLength()-1)
			{
				return false;
			}
		}
		if(location.getX() >= 0 && location.getX() <= 9 && location.getY() >= 0 && location.getY() <= 9)
		{
			myBackLocation=location;
			return true;	
		}
		else
		{
			return false;
		}
	}
	public Point getFrontLocation()
	{
		return myFrontLocation;
	}
	public void resetShip()
	{
		myFrontLocation=null;
		myBackLocation=null;
	}
	public Point getBackLocation()
	{
		return myBackLocation;
	}
	public ShipType getShipType()
	{
		return myShipType;
	}
	public int getLength()
	{
		return myLength;
	}
	public Vector<Boolean> getHits()
	{
		return myHits;
	}
}
