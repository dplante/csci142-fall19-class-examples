package battleship;

import java.awt.Point;
import java.util.HashMap;
import java.util.Vector;

public class DecisionMaker
{
	private ShipBoard myShipBoard;
	private Point myLastMove;
	private HashMap<String, Vector<Point>> myHashMap;
	private AILevel myAI;
	public DecisionMaker(AILevel level, ShipBoard board)
	{
		myAI=level;
		myShipBoard = board;
		myHashMap = new HashMap<>();
	}
	public Point nextMove()
	{
		if(myAI==AILevel.RANDOM_MODE)
		{
			while(true)
			{
				int randx=(int)(Math.random()*10);
				int randy=(int)(Math.random()*10);
				if(myShipBoard.isValidMove(new Point(randx, randy)))
				{
					return new Point(randx, randy);
				}
			}
		}
		if(myAI==AILevel.CHEAT_MODE)
		{
			Vector<Ship> temp=myShipBoard.getShips();
			for(int a=0; a<10; a++)
			{
				for(int b=0; b<10; b++)
				{
					Point point=new Point(a, b);
					if(myShipBoard.isValidMove(new Point(a, b)))
					{
						for(Ship i:temp)
						{
							boolean isvertical;
							if(i.getFrontLocation().getX()==i.getBackLocation().getX())
							{
								isvertical=true;
							}
							else
							{
								isvertical=false;
							}
							int frontx=(int)i.getFrontLocation().getX();
							int fronty=(int)i.getFrontLocation().getY();
							int backx=(int)i.getBackLocation().getX();
							int backy=(int)i.getBackLocation().getY();
							if(isvertical)
							{
								if(fronty<backy)
								{
									for(int y=fronty; y<=backy; y++)
									{
										if((point.getX()==frontx)&&(point.getY()==y))
										{
											return point;
										}
									}
								}
								else
								{
									for(int y=backy; y<=fronty; y++)
									{
										if((point.getX()==frontx)&&(point.getY()==y))
										{
											return point;
										}						
									}
								}
							}
							else
							{
								if(frontx<backx)
								{
									for(int x=frontx; x<=backx; x++)
									{
										if((point.getX()==x)&&(point.getY()==fronty))
										{
											return point;
										}
									}
								}
								else
								{
									for(int x=backx; x<=frontx; x++)
									{
										if((point.getX()==x)&&(point.getY()==fronty))
										{
											return point;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if(myAI==AILevel.INTELLIGENT_MODE)
		{
			if(myHashMap.isEmpty())
			{
				while(true)
				{
					int a=(int)(Math.random()*10);
					int b=(int)(Math.random()*10);
					Point p=new Point(a, b);
					if(myShipBoard.isValidMove(p))
					{
						return p;
					}
				}
			}
			else
			{
				//Vector<Point> a = null;
				Vector<Point> car=myHashMap.get("Carrier");
				Vector<Point> bat=myHashMap.get("Battleship");
				Vector<Point> cru=myHashMap.get("Cruiser");
				Vector<Point> sub=myHashMap.get("Submarine");
				Vector<Point> des=myHashMap.get("Destroyer");
				if(car!=null&&car.size()>0)
				{
					if(car.size()==1)
					{
						int hitx=(int)car.get(0).getX();
						int hity=(int)car.get(0).getY();
						if(hitx+1>=0&&hitx+1<=9&&hity>=0&&hity<=9)
							return new Point(hitx+1, hity);
						if(hitx>=0&&hitx<=9&&hity+1>=0&&hity+1<=9)
							return new Point(hitx, hity+1);
						if(hitx-1>=0&&hitx-1<=9&&hity>=0&&hity<=9)						
							return new Point(hitx-1, hity);
						return new Point(hitx, hity-1);
					}
					if(car.size()<5)
					{
						//IF SHIP IS VERTICAL
						if(car.get(0).getX()==car.get(1).getX())
						{
							int xp=(int)car.get(0).getX();
							//FIND LOWEST Y VALUE
							double min=10.0;
							for(Point p:car)
							{
								if(p.getY()<min)
									min=p.getY();
							}
							//FIND HIGHEST Y VALUE
							double max=-1.0;
							for(Point p:car)
							{
								if(p.getY()>max)
									max=p.getY();
							}
							if(car.size()==1+(int)max-(int)min)
							{
								if(xp>=0&&xp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point(xp, (int)max+1);
								}
								else
								{
									return new Point(xp, (int)min-1);
								}
							}
						}
						else
						{
							int yp=(int)car.get(0).getY();
							//FIND LOWEST X VALUE
							double min=10.0;
							for(Point p:car)
							{
								if(p.getX()<min)
									min=p.getX();
							}
							//FIND HIGHEST X VALUE
							double max=-1.0;
							for(Point p:car)
							{
								if(p.getX()>max)
									max=p.getX();
							}
							if(car.size()==1+(int)max-(int)min)
							{
								if(yp>=0&&yp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point((int)max+1, yp);
								}
								else
								{
									return new Point((int)min-1, yp);
								}
							}
						}
					}
				}
				if(bat!=null&&bat.size()>0)
				{
					if(bat.size()==1)
					{
						int hitx=(int)bat.get(0).getX();
						int hity=(int)bat.get(0).getY();
						if(hitx+1>=0&&hitx+1<=9&&hity>=0&&hity<=9)
							return new Point(hitx+1, hity);
						if(hitx>=0&&hitx<=9&&hity+1>=0&&hity+1<=9)
							return new Point(hitx, hity+1);
						if(hitx-1>=0&&hitx-1<=9&&hity>=0&&hity<=9)						
							return new Point(hitx-1, hity);
						return new Point(hitx, hity-1);
					}
					if(bat.size()<4)
					{
						//IF SHIP IS VERTICAL
						if(bat.get(0).getX()==bat.get(1).getX())
						{
							int xp=(int)bat.get(0).getX();
							//FIND LOWEST Y VALUE
							double min=10.0;
							for(Point p:bat)
							{
								if(p.getY()<min)
									min=p.getY();
							}
							//FIND HIGHEST Y VALUE
							double max=-1.0;
							for(Point p:bat)
							{
								if(p.getY()>max)
									max=p.getY();
							}
							if(bat.size()==1+(int)max-(int)min)
							{
								if(xp>=0&&xp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point(xp, (int)max+1);
								}
								else
								{
									return new Point(xp, (int)min-1);
								}
							}
						}
						else
						{
							int yp=(int)bat.get(0).getY();
							//FIND LOWEST X VALUE
							double min=10.0;
							for(Point p:bat)
							{
								if(p.getX()<min)
									min=p.getX();
							}
							//FIND HIGHEST X VALUE
							double max=-1.0;
							for(Point p:bat)
							{
								if(p.getX()>max)
									max=p.getX();
							}
							if(bat.size()==1+(int)max-(int)min)
							{
								if(yp>=0&&yp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point((int)max+1, yp);
								}
								else
								{
									return new Point((int)min-1, yp);
								}
							}
						}
					}
				}
				if(cru!=null)
				{
					if(cru.size()==1)
					{
						int hitx=(int)cru.get(0).getX();
						int hity=(int)cru.get(0).getY();
						if(hitx+1>=0&&hitx+1<=9&&hity>=0&&hity<=9)
							return new Point(hitx+1, hity);
						if(hitx>=0&&hitx<=9&&hity+1>=0&&hity+1<=9)
							return new Point(hitx, hity+1);
						if(hitx-1>=0&&hitx-1<=9&&hity>=0&&hity<=9)						
							return new Point(hitx-1, hity);
						return new Point(hitx, hity-1);
					}
					if(cru.size()<4)
					{
						//IF SHIP IS VERTICAL
						if(cru.get(0).getX()==cru.get(1).getX())
						{
							int xp=(int)cru.get(0).getX();
							//FIND LOWEST Y VALUE
							double min=10.0;
							for(Point p:cru)
							{
								if(p.getY()<min)
									min=p.getY();
							}
							//FIND HIGHEST Y VALUE
							double max=-1.0;
							for(Point p:cru)
							{
								if(p.getY()>max)
									max=p.getY();
							}
							if(cru.size()==1+(int)max-(int)min)
							{
								if(xp>=0&&xp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point(xp, (int)max+1);
								}
								else
								{
									return new Point(xp, (int)min-1);
								}
							}
						}
						else
						{
							int yp=(int)cru.get(0).getY();
							//FIND LOWEST X VALUE
							double min=10.0;
							for(Point p:cru)
							{
								if(p.getX()<min)
									min=p.getX();
							}
							//FIND HIGHEST X VALUE
							double max=-1.0;
							for(Point p:cru)
							{
								if(p.getX()>max)
									max=p.getX();
							}
							if(cru.size()==1+(int)max-(int)min)
							{
								if(yp>=0&&yp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point((int)max+1, yp);
								}
								else
								{
									return new Point((int)min-1, yp);
								}
							}
						}
					}
				}
				if(sub!=null)
				{
					if(sub.size()==1)
					{
						int hitx=(int)sub.get(0).getX();
						int hity=(int)sub.get(0).getY();
						if(hitx+1>=0&&hitx+1<=9&&hity>=0&&hity<=9)
							return new Point(hitx+1, hity);
						if(hitx>=0&&hitx<=9&&hity+1>=0&&hity+1<=9)
							return new Point(hitx, hity+1);
						if(hitx-1>=0&&hitx-1<=9&&hity>=0&&hity<=9)						
							return new Point(hitx-1, hity);
						return new Point(hitx, hity-1);
					}
					if(sub.size()<4)
					{
						//IF SHIP IS VERTICAL
						if(sub.get(0).getX()==sub.get(1).getX())
						{
							int xp=(int)sub.get(0).getX();
							//FIND LOWEST Y VALUE
							double min=10.0;
							for(Point p:sub)
							{
								if(p.getY()<min)
									min=p.getY();
							}
							//FIND HIGHEST Y VALUE
							double max=-1.0;
							for(Point p:sub)
							{
								if(p.getY()>max)
									max=p.getY();
							}
							if(sub.size()==1+(int)max-(int)min)
							{
								if(xp>=0&&xp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point(xp, (int)max+1);
								}
								else
								{
									return new Point(xp, (int)min-1);
								}
							}
						}
						else
						{
							int yp=(int)sub.get(0).getY();
							//FIND LOWEST X VALUE
							double min=10.0;
							for(Point p:sub)
							{
								if(p.getX()<min)
									min=p.getX();
							}
							//FIND HIGHEST X VALUE
							double max=-1.0;
							for(Point p:sub)
							{
								if(p.getX()>max)
									max=p.getX();
							}
							if(sub.size()==1+(int)max-(int)min)
							{
								if(yp>=0&&yp<=9&&(int)max+1<=9&&(int)max+1>=0)
								{
									return new Point((int)max+1, yp);
								}
								else
								{
									return new Point((int)min-1, yp);
								}
							}
						}
					}
				}
				if(des!=null)
				{
					if(des.size()==1)
					{
						int hitx=(int)des.get(0).getX();
						int hity=(int)des.get(0).getY();
						if(hitx+1>=0&&hitx+1<=9&&hity>=0&&hity<=9)
							return new Point(hitx+1, hity);
						if(hitx>=0&&hitx<=9&&hity+1>=0&&hity+1<=9)
							return new Point(hitx, hity+1);
						if(hitx-1>=0&&hitx-1<=9&&hity>=0&&hity<=9)						
							return new Point(hitx-1, hity);
						return new Point(hitx, hity-1);
					}
				}
			}
		}
		return null;
	}
	public void placeNextMove()
	{
		
	}
	public AILevel getLevel()
	{
		return null;
	}
	public void setLastMove(Point point)
	{
		myLastMove = point;
	}
	public Point getLastMove()
	{
		return myLastMove;
	}
	public ShipBoard getShipBoard()
	{
		return myShipBoard;
	}
	public void setHashMap(HashMap<String, Vector<Point>> map)
	{
		myHashMap = map;
	}
	public HashMap<String, Vector<Point>> getHashMap()
	{
		return myHashMap;
	}
}
