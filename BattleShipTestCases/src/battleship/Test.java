package battleship;

import java.awt.Point;

public class Test {

	public static void main(String[] args) {
		Point point1 = new Point(4, 5);
		Point point2 = new Point(4, 6);
		Point point3 = new Point(4, 6);
		
		/*
		 * Compare objects with equal(), not ==
		 */
		if (point1.equals(point2)) System.out.println("1=2");
		if (point3.equals(point2)) System.out.println("3=2");
		if (point3 == point2) System.out.println("3=2");

		if (point3.x == point2.x && point3.y == point2.y) System.out.println("3=2");
	}

}
