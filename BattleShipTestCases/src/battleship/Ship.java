package battleship;

import java.awt.Point;
import java.util.Vector;

public class Ship
{
	private Point myFrontLocation;
	private Point myBackLocation;
	private int myLength;
	private Vector<Boolean> myHits;
	private boolean mySunk;
	
	public Ship(ShipType ship, int length)
	{
		myLength = length;
		myHits = new Vector<Boolean>();
		for(int i = 0; i < myLength; ++i)
		{
			myHits.add(false);
		}
		myFrontLocation = null;
		myBackLocation = null;
		mySunk = false;
	}
	public boolean placePeg(Point location)
	{
		return false;
	}
	public boolean isSunk()
	{
		return mySunk;
	}
	public boolean setFrontLocation(Point location)
	{
		if(location.getX() >= 0 && location.getX() <= 9 && location.getY() >= 0 && location.getY() <= 9)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}
	public boolean setBackLocation(Point location)
	{
		if(location.getX() >= 0 && location.getX() <= 9 && location.getY() >= 0 && location.getY() <= 9)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}
	public Point getFrontLocation()
	{
		return myFrontLocation;
	}
	public Point getBackLocation()
	{
		return myBackLocation;
	}
	public ShipType getShipType()
	{
		return null;
	}
	public int getLength()
	{
		return myLength;
	}
	public Vector<Boolean> getHits()
	{
		return myHits;
	}
}
