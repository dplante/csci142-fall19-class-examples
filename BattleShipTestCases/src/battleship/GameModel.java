package battleship;

public class GameModel 
{
	private ShipBoard myShipBoard;
	private ShipBoard myComputerBoard;
	private String myPlayerName;
	private boolean myIsComputerTurn;
	private String myMessage;
	private AILevel myAILevel;
	
	public GameModel(String name, AILevel level)
	{
		
	}
	public void beginGame()
	{
		
	}
	public boolean checkIfWinner()
	{
		return false;
	}
	public String generateMessage()
	{
		return null;
	}
	public ShipBoard getShipBoard()
	{
		return myShipBoard;
	}
	public ShipBoard getComputerBoard()
	{
		return myComputerBoard;
	}
	public String getPlayerName()
	{
		return myPlayerName;
	}
	public boolean getIsComputerTurn()
	{
		return myIsComputerTurn;
	}
	public String getMessage()
	{
		return myMessage;
	}
	public AILevel getAILevel()
	{
		return myAILevel;
	}
}
