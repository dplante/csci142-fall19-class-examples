package battleship;

import java.awt.Point;
import java.util.Vector;

public class ShipBoard 
{
	public final int SIZE = 10;
	
	private PegType[][] myGrid;
	private PegType myLastMove;
	
	public ShipBoard()
	{
		myGrid = new PegType[SIZE][SIZE];
		myLastMove = PegType.NONE;
	}
	public boolean placePeg(Point point)
	{
		return false;
	}
	public boolean isValidMove(Point point)
	{
		return false;
	}
	public boolean placeShips()
	{
		return false;
	}
	public void resetBoard()
	{
		
	}
	public boolean placeShip(ShipType type, Point front, Point back)
	{
		return false;
	}
	public int numberShipsSunk()
	{
		return 0;
	}
	public boolean allShipsSunk()
	{
		return false;
	}
	public PegType getLastMove()
	{
		return myLastMove;
	}
	public PegType[][] getGrid()
	{
		return myGrid;
	}
	public Vector<Ship> getShips()
	{
		return null;
	}
}
