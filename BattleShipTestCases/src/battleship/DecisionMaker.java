package battleship;

import java.awt.Point;
import java.util.HashMap;
import java.util.Vector;

public class DecisionMaker
{
	private ShipBoard myShipBoard;
	private Point myLastMove;
	private HashMap<String, Vector<Point>> myHashMap;
	
	public DecisionMaker(AILevel level, ShipBoard board)
	{
		myShipBoard = board;
		myHashMap = new HashMap<>();
	}
	public Point nextMove()
	{
		return null;
	}

	public AILevel getLevel()
	{
		return null;
	}
	public void setLastMove(Point point)
	{
		myLastMove = point;
	}
	public Point getLastMove()
	{
		return myLastMove;
	}
	public ShipBoard getShipBoard()
	{
		return myShipBoard;
	}
	public void setHashMap(HashMap<String, Vector<Point>> map)
	{
		myHashMap = map;
	}
	public HashMap<String, Vector<Point>> getHashMap()
	{
		return myHashMap;
	}
}
