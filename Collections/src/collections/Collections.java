package collections;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.Vector;

public class Collections 
{

	public static void main(String[] args) 
	{
		Vector<String> data = new Vector<String>();
		
		/*
		 * Add the "Computer Science Super Heros" to the data structure
		 */
		data.add(new String("Leah"));
		data.add("Rachel");
		data.add("Stephen");
		data.add(new String("Carson"));
		
		data.insertElementAt("Evan", 2);
		
		System.out.println("Size = " + data.size());

		/*
		 * The way you have traditionally traversed a
		 * data structure.
		 */
		for (int i=0; i<data.size(); i++)
		{
			System.out.println(data.elementAt(i));
		}
		
		/*
		 * Traverse data structure with an iterator
		 */
//		Iterator<String> iterator = data.iterator();
//
//		while (iterator.hasNext())
//		{
//			System.out.println(iterator.next());
//			//iterator.remove();
//		}
		
//		System.out.println("\nStack Stuff");
//		while (!data.empty())
//		{
//			System.out.println(data.pop());
//		}
//		
//		System.out.println("Size = " + data.size());
		
		/*
		 * "Autoboxing" means you can add a "non-object" when
		 * the data structure expects an object, but it "auto-boxes"
		 * it from a simple type to an object.
		 */
		//data.add(new Integer(4));
		//data.add(4);
	}

}
