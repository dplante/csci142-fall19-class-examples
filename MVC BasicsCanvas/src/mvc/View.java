package mvc;
/**
 * Simple view class used to show how Model-View-Controller
 * is implemented.  This is also the main application.
 * Version 2.0 uses Generics.
 *
 * @author Daniel Plante
 * @version 1.0 (28 January 2002)
 * @version 2.0 (1 February 2008)
 */

import java.awt.*;
import java.lang.reflect.*;

import javax.swing.JFrame;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class View 
{
    //////////////////////
    //    Properties    //
    //////////////////////
    
	private Can myButtonIncrement;
    private Canvas myButtonDecrement;
    private ButtonListener myIncrementListener;
    private ButtonListener myDecrementListener;
    private JTextField myTextField;
    private Controller myController;
    private Image myImage;
    
    ///////////////////////
    //      Methods      //
    ///////////////////////
    
    /**
     * View constructor used to lay out the view
     *
     * <pre>
     * pre:  none
     * post: the view is set up and initialized
     * </pre>
     */
    public View(Controller controller)
    {
        String value;
        
        JFrame frame = new JFrame("MVC Basics with Canvas");
        
        frame.setSize(300,300);
        frame.setLayout(new FlowLayout());
        frame.setBackground(Color.gray);
        
        myController = controller;
        
        this.associateListeners();
        
        myImage = Toolkit.getDefaultToolkit().getImage("images/card.jpg");
        myButtonIncrement = new Can(myImage);
        myButtonDecrement = new Can("Hi");
        
        myButtonIncrement.addMouseListener(myIncrementListener);
        myButtonDecrement.addMouseListener(myDecrementListener);
        
        value = myController.getModelValue();
        myTextField = new JTextField(value);
        
        // set size and background color of can (canvas)
        myButtonIncrement.setBackground(Color.cyan);
        myButtonDecrement.setBackground(Color.green);
        myButtonIncrement.setSize(79,113);
        myButtonDecrement.setSize(50,50);
        
        frame.add(myButtonIncrement);
        frame.add(myButtonDecrement);
        frame.add(myTextField);
        
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Associates each component's listener with the controller
     * and the correct method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    public void associateListeners()
    {
        Class<? extends Controller> controllerClass;
        Method incrementMethod,
               decrementMethod;
               
        controllerClass = myController.getClass();
        
        incrementMethod = null;
        decrementMethod = null;
        
        try
        {
           incrementMethod = controllerClass.getMethod("increment",(Class<?>[])null);
           decrementMethod = controllerClass.getMethod("decrement",(Class<?>[])null);        
        }
        catch(NoSuchMethodException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        
        myIncrementListener = new ButtonListener(myController, incrementMethod);
        myDecrementListener = new ButtonListener(myController, decrementMethod);
    }
    
    /**
     * Updates myTextField with the String text.
     *
     * @param text the text string to use in updating the text field
     */
    public void setTextField(String text)
    {
        myTextField.setText(text);
    }
    
}