package structures;

import java.awt.Point;
import java.util.HashMap;
import java.util.Vector;

public class HashMapExample 
{

	public static void main(String[] args) 
	{
		HashMap<String, Vector<Point>> map = new HashMap<>();

		map.put("Battleship", new Vector<Point>());
		map.put("Cruiser", new Vector<Point>());
		
		map.get("Battleship").add(new Point(1,2));
		map.get("Battleship").add(new Point(1,3));
		
		map.get("Cruiser").add(new Point(4,5));
		map.get("Cruiser").add(new Point(5,5));
		map.get("Cruiser").remove(1);
		
		System.out.println(map);

		
	}

}
