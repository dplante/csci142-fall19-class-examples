package poker.model;

public enum PokerHandRanking
{
	HIGH_CARD (1), 
	PAIR (2),
	TWO_PAIR (3),
	THREE_OF_KIND (4), 
	ROYAL_FLUSH (10);
	
	private final int myRank;
	
	private PokerHandRanking(int rank)
	{
		myRank = rank;
	}
	public int getRank() 
	{ 
		return myRank; 
	}
}
