package poker.model;

import java.util.Vector;

/**
 * Example of using CategoryType enum (enumeration) to
 * provide type safety compared to many "final static int" 
 * values being set for the different categories of the
 * Poker game.
 * 
 * @author dplante
 *
 */
public class PokerHand 
{
	private PokerHandRanking myType;
	
	public PokerHand(PokerHandRanking type)
	{
		myType = type;
	}

	public PokerHandRanking getType() {
		return myType;
	}

	public void setType(PokerHandRanking type) {
		myType = type;
	}

	public static void main(String[] args)
	{
		Vector<PokerHand> vector = new Vector<PokerHand>();
		vector.add(new PokerHand(PokerHandRanking.PAIR));
		vector.add(new PokerHand(PokerHandRanking.ROYAL_FLUSH));
		
		for(Object cat : vector)
		{
			System.out.println(((PokerHand)cat).getType());
			System.out.println(((PokerHand)cat).getType().getRank());
		}
	}
}
