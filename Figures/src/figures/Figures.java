package figures;

public class Figures 
{

	public static void main(String[] args) 
	{
		Square square1;
		Square square2;
		
		square1 = new Square();
		square2 = new Square(4.0f);
		
		/*
		 * Whoops! Can call setSideLength() or setArea() and break things!
		 */
		square1.setSideLength(10.0f);
		System.out.println("Square1: sidelength = " + square1.getSideLength()
							+ "    area = " + square1.getMyArea());
		System.out.println("Square2: sidelength = " + square2.getSideLength()
							+ "    area = " + square2.getMyArea());

	}

}
