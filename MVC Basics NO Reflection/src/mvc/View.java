package mvc;
/**
 * Simple view class used to show how Model-View-Controller
 * is implemented.  This is also the main application. 
 * Version 2.0 uses Generics.
 *
 * @author Daniel Plante
 * @version 1.0 (28 January 2002)
 * @version 2.0 (1 February 2008)
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class View
{
	//////////////////////
    //    Properties    //
    //////////////////////
    
	private JButton myButtonIncrement;
    private JButton myButtonDecrement;
    private IncrementListener myIncrementListener;
    private DecrementListener myDecrementListener;
    private JTextField myTextField;
    
    ///////////////////////
    //      Methods      //
    ///////////////////////
    
    /**
     * View constructor used to lay out the view
     *
     * <pre>
     * pre:  none
     * post: the view is set up and initialized
     * </pre>
     */
    public View(Controller controller)
    {
		JFrame frame = new JFrame("MVC Basics");

		String value;
       
        frame.setSize(300,100);
        frame.setLayout(new FlowLayout());
        this.associateListeners(controller);
        
        myButtonIncrement = new JButton("+");
        myButtonDecrement = new JButton("-");
        
        myButtonIncrement.addActionListener(myIncrementListener);
        myButtonDecrement.addActionListener(myDecrementListener);
        
        value = controller.getModelValue();
        myTextField = new JTextField(value);
        
        frame.add(myButtonIncrement);
        frame.add(myButtonDecrement);
        frame.add(myTextField);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Associates each component's listener with the controller
     * and the correct method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has to be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    public void associateListeners(Controller controller)
    {
        myIncrementListener = new IncrementListener(controller);
        myDecrementListener = new DecrementListener(controller);
    }
    
    /*
     * Private listener class for increment() method in controller class.
     */
    private class IncrementListener implements ActionListener
    {
	    	private Controller myController;
	    	public IncrementListener(Controller controller)
	    	{
	    		myController = controller;
	    	}

		public void actionPerformed(ActionEvent event) 
		{
			myController.increment();
		}
    }
  
    /*
     * Private listener class for decrement() method in controller class.
     */   
    private class DecrementListener implements ActionListener
    {
    	private Controller myController;
    	public DecrementListener(Controller controller)
    	{
    		myController = controller;
    	}

		public void actionPerformed(ActionEvent event) 
		{
			myController.decrement();
		}
    }
    
    /**
     * Updates myTextField with the String text.
     *
     * @param text the text string to use in updating the text field
     */
    public void setTextField(String text)
    {
        myTextField.setText(text);
    }
}