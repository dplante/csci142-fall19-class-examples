package figures;

/**
 * Abstract Shape class implements everything it can "concretely"
 * (e.g. getType() and getArea()) but abstractly forces any 
 * subclass to implement its abstract methods (e.g. toString())
 * 
 * @author dplante
 *
 */
public abstract class Shape 
{
	protected float myArea;
	protected String myType;
	
	public abstract String toString();

	public float getArea() 
	{
		return myArea;
	}

	public String getType() 
	{
		return myType;
	}

	public void setType(String type) 
	{
		myType = type;
	}
}
