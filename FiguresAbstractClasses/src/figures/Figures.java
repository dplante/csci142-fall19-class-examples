package figures;

/**
 * The main Figures program that uses, for example,
 * the Square class we write.
 * 
 * @author dplante
 * @version 0.00001
 */
public class Figures 
{

	public static void main(String[] args) 
	{
		Square square1;
		Square square2;
		
		square1 = new Square();
		square2 = new Square(7.0f);
		square2.setSideLength(5.0f);
		
		System.out.println(square1);
		System.out.println(square2);

	}

}
