package structures;
import java.util.LinkedList;

public class Queue<T>
{
	private LinkedList<T> myData;
	
	public Queue()
	{
		myData = new LinkedList<T>();
	}
	
	public boolean enqueue(T o)
	{
		return myData.offer(o);
	}
	
	public T dequeue()
	{
		return myData.poll();
	}
	
	public T peek()
	{
		return myData.peek();
	}
	
	public boolean empty()
	{
		return myData.isEmpty();
	}
}
