package structures;

import java.util.LinkedList;

public class TestQueue {

	public static void main(String[] args) {
		Queue<String> queue = new Queue<String>();
		queue.enqueue("Dani Frodo");
		queue.enqueue("Pablo The Tall One");
		queue.enqueue("Gandolf the Rachel");
		queue.enqueue("Samwise Coley");
		
		while(!queue.empty())
		{
			System.out.println(queue.dequeue());
		}
		
		/*
		 * Could have done this instead!!!!
		 * 
		 * Punchline: This is not ideal for a queue because
		 * you can do anything with it, while the above queue
		 * is a "wrapper" around LinkedList that only exposes 
		 * the appropriate functionality!!
		 * 
		 * This is important, complain to Sun/Oracle that they
		 * did not do this when writing the Stack which extends
		 * Vector!!  Bad bad bad.
		 */
		LinkedList<String> ll = new LinkedList<String>();
		ll.offer("Neo Kenneth");
		ll.offer("Oracle Leah");
		ll.offer("Morpheus Joey");
		
		while(!ll.isEmpty())
		{
			System.out.println(ll.poll());
		}
	}

}
