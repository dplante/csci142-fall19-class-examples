package expressiontrees;
/**
 * Class:  BinaryTreeTests
 * @author Daniel Plante
 * @version 1.0
 */

public class BinaryTreeTests
{
    private BinaryTree myTree;
    
    public static void main(String[] args)
    {
        new BinaryTreeTests();
    }
    
    public BinaryTreeTests()
    {
        BinaryTree leftTree, rightTree;
        
        leftTree = new BinaryExpressionTree("15", null, null);
        rightTree = new BinaryExpressionTree("3", null, null);
        rightTree = new BinaryExpressionTree("/", leftTree, rightTree);
        leftTree = new BinaryExpressionTree("8", null, null);
        leftTree = new BinaryExpressionTree("+", leftTree, rightTree);
        rightTree = new BinaryExpressionTree("4", null, null);
        myTree = new BinaryExpressionTree("-", leftTree, rightTree);
		System.out.println("In Order:");
		myTree.traverseInOrder();
		System.out.println("\nIn Order Euler:");
		myTree.traverseInOrderEuler();
		System.out.println("\nPre Order:");
		myTree.traversePreOrder();
		System.out.println("\nPost Order:");
		myTree.traversePostOrder();
    }
}