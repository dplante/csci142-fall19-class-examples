package expressiontrees;
/**
 * Class:   BinaryTree
 * Purpose: To illustrate different traversals of BinaryTrees.
 *          This is NOT the best way to do it; should use iterators
 * 
 * @author  Daniel Plante
 * @version 1.0
 */

public abstract class BinaryTree
{
   //////////////////////////////////////////////
   //            Properties                    //
   //////////////////////////////////////////////
   private BinaryTree myLeftTree;
   private BinaryTree myRightTree;
   private BinaryTree myParent;
   private Object myData;

   //////////////////////////////////////////////
   //            Methods                       //
   //////////////////////////////////////////////
   public BinaryTree()
   {
      myParent = null;
      myData = null;
      myLeftTree = null;
      myRightTree = null;
   }
  
   public BinaryTree(Object datum, BinaryTree left, BinaryTree right)
   {
      myParent = null;
      this.setData(datum);
      this.setLeftTree(left);
      this.setRightTree(right);
   }

   //////////////////////
   // Abstract method  //
   //////////////////////
   public abstract void visit(Object o);

   //////////////////////
   // Concrete methods //
   //////////////////////
   public void traverseInOrder()
   {
      this.traverseInOrder(this, false);
   }
   
   public void traverseInOrderEuler()
   {
   	  this.traverseInOrder(this, true);
   }
   
   private void traverseInOrder(BinaryTree tree, boolean euler)
   {
      Object datum;
      BinaryTree left, right;
      
      if(tree != null)
      {
         datum = tree.getData();
         left = tree.getLeftTree();
         right = tree.getRightTree();
         if(left != null && right != null && euler)
         {
            this.visit("(");
         }
         this.traverseInOrder(left, euler);
         this.visit(datum);
         this.traverseInOrder(right, euler);
         if(left != null && right != null && euler)
         {
            this.visit(")");
         }
      }
   }

   public void traversePostOrder()
   {
      this.traversePostOrder(this);
   }
   
   private void traversePostOrder(BinaryTree tree)
   {
      Object datum;
      
      if(tree != null)
      {
         datum = tree.getData();
         this.traversePostOrder(tree.getLeftTree());
         this.traversePostOrder(tree.getRightTree());
         this.visit(datum);
      }
   }

   public void traversePreOrder()
   {
      this.traversePreOrder(this);
   }
   
   private void traversePreOrder(BinaryTree tree)
   {
      Object datum;
      
      if(tree != null)
      {
         datum = tree.getData();
         this.visit(datum);
         this.traversePreOrder(tree.getLeftTree());
         this.traversePreOrder(tree.getRightTree());
      }
   }

   public boolean hasLeftTree()
   {
      BinaryTree leftTree;
    
      leftTree = this.getLeftTree();
      return leftTree != null;
   }
  
   public boolean hasRightTree()
   {
      BinaryTree rightTree;
    
      rightTree = this.getRightTree();
      return rightTree != null;
   }
  
   public void setLeftTree(BinaryTree tree)
   {
      BinaryTree leftTree, leftParent;
    
      leftTree = null;
    
      if(this.hasLeftTree())
      {
         leftTree = this.getLeftTree();
         leftParent = leftTree.getParent();
         if (leftParent == this)
         {
            leftTree.setParent(null);
         }
      }
      
      myLeftTree = tree;
      if(tree != null)
      {
         tree.setParent(this);
      }
   }

   public void setRightTree(BinaryTree tree)
   {
      BinaryTree rightTree, rightParent;
    
      rightTree = null;
    
      if(this.hasRightTree())
      {
         rightTree = this.getRightTree();
         rightParent = rightTree.getParent();
         if (rightParent == this)
         {
            rightTree.setParent(null);
         }
      }
      
      myRightTree = tree;
      if(tree != null)
      {
         tree.setParent(this);
      }
   }

   public BinaryTree getLeftTree()
   {
      return myLeftTree;
   }

   public BinaryTree getRightTree()
   {
      return myRightTree;
   }

   public void setParent(BinaryTree tree)
   {
      myParent = tree;
   }

   public BinaryTree getParent()
   {
      return myParent;
   }

   public void setData(Object data)
   {
      myData = data;
   }

   public Object getData()
   {
      return myData;
   }
}