package expressiontrees;
/**
 * Class:  BinaryExpressionTree
 * @author Daniel Plante
 * @version 1.0
 */

public class BinaryExpressionTree extends BinaryTree
{
   //////////////////////////////////////////////
   //            Properties                    //
   //////////////////////////////////////////////

   //////////////////////////////////////////////
   //            Methods                       //
   //////////////////////////////////////////////
   public BinaryExpressionTree()
   {
      super();
   }

   public BinaryExpressionTree(Object datum, BinaryTree left, BinaryTree right)
   {
      super(datum, left, right);
   }

   public void visit(Object datum)
   {
      if (datum != null)
      {
        System.out.print(" " + datum + " ");
      }
   }
}
