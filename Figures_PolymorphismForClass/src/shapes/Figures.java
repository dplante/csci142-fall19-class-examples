package shapes;
/**
 * A Figures project with interfaces
 * 
 * @author dplante
 *
 */
public class Figures 
{

	public static void main(String[] args) 
	{
		Shape[] shapes = new Shape[4];
		
		shapes[0] = new Square();
		shapes[1] = new Square(3.0f);
		shapes[2] = new Rectangle();
		shapes[3] = new Rectangle(2.0f, 6.0f); 
		
		/*
		 * Uncomment each of the four lines below.  What happens and why?
		 * How do you fix these?
		 */
		System.out.println((shapes[1]).getArea());
		System.out.println(((Square)shapes[1]).getSideLength());
		System.out.println((shapes[2]).getArea());
		System.out.println(((Rectangle)shapes[2]).getWidth());

		/*
		 * Can you write a loop that goes through the shapes and prints 
		 * either the side length of the shape if it is a square, or the 
		 * width of the shape if it is a rectangle?
		 *
		 * HINT: what does "System.out.println(shapes[1] instanceof Square);" print?
		 */
		for (int i=0; i<4; i++)
		{
			//System.out.println(shapes[i]);
			if(shapes[i] instanceof Square)
			{
				System.out.println("Square sidelength = " + ((Square)shapes[i]).getSideLength());
			}
			else
			{
				System.out.println("Rectangle width = " + ((Rectangle)shapes[i]).getWidth());
			}
		}
		
		float totalArea = 0;
		for (Shape shape : shapes)
		{
			totalArea += shape.getArea();
		}
		System.out.println("Total Area: " + totalArea);
	}
}
