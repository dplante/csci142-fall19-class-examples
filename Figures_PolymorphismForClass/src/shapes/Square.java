package shapes;
/**
 * A better Square class
 * 
 * @author dplante
 *
 */
public class Square implements Shape
{
	private float myArea;

	/*
	 * fields, properties, attributes
	 */
	private float mySideLength;
	
	/*
	 * methods
	 */
	public Square()
	{
		this(1.0f);
	}
	
	public Square(float sideLength)
	{
		/*
		 * My coding standard is to NOT include this
		 * for properties, but to include it for methods.
		 */
		this.mySideLength = sideLength;
		this.calculateArea();
	}
	
	public void calculateArea()
	{
		/* 
		 * Don't use Math.pow() for something this simple.
		 */
		//myArea = (float)Math.pow(sideLength, 2.0);
		myArea = mySideLength * mySideLength;
	}
	
	public String toString()
	{
		String string = this.getClass().getName()
				+ ": sideLength = " + this.getSideLength() 
				+ "  area = " + this.getArea();
		return string;
	}

	/*
	 * Setters and Getters
	 * Accessors and Mutators
	 */
	public float getSideLength() 
	{
		return mySideLength;
	}

	public void setSideLength(float sideLength) 
	{
		mySideLength = sideLength;
		this.calculateArea();
	}
	
	public float getArea() 
	{
		return myArea;
	}
}
