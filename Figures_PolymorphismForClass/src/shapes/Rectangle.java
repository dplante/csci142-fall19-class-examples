package shapes;
/**
 * Rectangle class that adheres to the Shape interface (design by contract)
 * 
 * @author Daniel Plante
 *
 */
public class Rectangle implements Shape
{
	// Properties
	private float myArea;
	private float myLength,
				  myWidth;
   
	// Methods
	/**
	 * Default constructor for creating a default 1X1 rectangle
	 * which is analogous to a square.
	 */
	public Rectangle()
	{
		this(1, 1);
	}
	
	/**
	 * Constructor for creating a rectangle with specified 
	 * length and width.
	 * 
	 * @param width side width of the rectangle
	 * @param length side length of the rectangle
	 */
	public Rectangle(float width, float length)
	{
		myWidth = width;
		myLength = length;
		this.calculateArea();
	}
   
	private void calculateArea()
	{
	   myArea  = myLength * myWidth;
	}

	@Override
	/**
	 * Override of Object's toString() to provide the length,
	 * width, and area of the rectangle.
	 */
	public String toString() 
	{
		return "Rectangle [myArea=" + myArea + ", myLength=" + myLength
				+ ", myWidth=" + myWidth + "]";
	}

	/*
	 * Accessor and mutator methods 
	 * (accessor and mutator methods go at the end of class and are kept together. 
     */
	
	/**
	 * Set the length of the rectangle with the area updated for consistency.
	 * 
	 * @param length side length of rectangle
	 */
	public void setLength(float length)
	{
		myLength = length;
	    this.calculateArea();
	}

	/**
	 * Get side length of rectangle
	 * 
	 * @return side length of rectangle
	 */
	public float getLength()
	{
		return myLength;
	}

	/**
	 * Set the width of the rectangle with the area updated for consistency.
	 * 
	 * @param width width of rectangle
	 */
    public void setWidth(float width)
    {
	   myWidth = width;
	   this.calculateArea();
    }

    /**
     * Get width of rectangle
     * 
     * @return width of rectangle
     */
    public float getWidth()
    {
	   return myWidth;
    }

    /**
     * Get area of rectangle
     * 
     * @return area of rectangle
     */
    public float getArea()
    {
	   return myArea;
    }
}
