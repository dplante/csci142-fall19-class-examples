package testing;

/**
 * Test storage but only a little :)
 * 
 * @author dplante
 */
import storage.*;

public class TestStorage
{

    public static void main(String[] args)
    {
        Storage<Float> storage = new VectorStorage<Float>();
        
        //storage.add("Kassi");
        //storage.add(new String("Ruby"));
        storage.add(42.0f);
        System.out.println(storage);

        try
        {
            storage.remove(0);
            System.out.println(storage);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println(e.getMessage());
        }

    }

}
