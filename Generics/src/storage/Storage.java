package storage;

/**
 * Interface to abstract the adding and removing of items to 
 * storage, using "generics"
 * 
 * @author dplante
 *
 * @param <T>
 */
public interface Storage<T>
{
    public void add(T o);
    public T remove(int index);
    public String toString();

}
