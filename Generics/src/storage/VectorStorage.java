package storage;

/**
 * A Vector storage class.  Note that we could also create
 * a database storage, or cloud storage, or other storage
 * class that implements Storage<T> generically!
 */
import java.util.Vector;

public class VectorStorage<T extends Number> implements Storage<T>
{
    private Vector<T> myData;
    
    public VectorStorage()
    {
        myData = new Vector<T>();
    }

    @Override
    public void add(T o)
    {
        myData.add(o);
        
    }

    @Override
    public T remove(int index) throws ArrayIndexOutOfBoundsException
    {
        return myData.remove(index);
    }
    
    @Override
    public String toString()
    {
        String string="";
        
        for(T o:myData)
        {
            string += o;
            string += " ";
        }
        return string;
    }

}
