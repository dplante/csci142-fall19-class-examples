package structures;

import java.util.Vector;

public class Vectors 
{

	public static void main(String[] args) 
	{
		/*
		 * By default, instantiating a vector with the default constructor,
		 * 
		 * Vector data = new Vector();
		 * 
		 * creates a vector of capacity 10 and doubles when capacity is passed
		 * 
		 * Also can pass two parameters:
		 * 
		 * Vector data = new Vector(10,5);
		 * 
		 * where first number is the initial capacity and the second is
		 * the additional number of locations allocated, not doubled.
		 */
		
		Vector data = new Vector(2);
		
		System.out.println("size = " + data.size() + "  capacity = " + data.capacity());
		
		for (int i = 1; i<10; i++)
		{
			data.add("I love this class!");
		}
		
		data.add(new String("Stephen"));
		data.add("Leah");
		data.add(new Integer(4));
		data.add(7.9f);
		data.add(8.8888);
		
		System.out.println("size = " + data.size() + "  capacity = " + data.capacity());
		
	}

}
