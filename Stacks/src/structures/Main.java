package structures;

import java.util.NoSuchElementException;
import java.util.Stack;

public class Main 
{

	public static void main(String[] args) 
	{
		//Stack<String> stack = new Stack<String>();
		StackVector<String> stack = new StackVector<String>();
		//StackLL<String> stack = new StackLL<String>();
		
		//System.out.println(stack.pop());
		
		stack.push("Tired");
		stack.push("Students");
		stack.push("Love");
		stack.push("Sleeping");
		
		System.out.println("Stack empty? " + stack.empty());
		while (!stack.empty())
		{
			System.out.println(stack.pop());
		}
//		try{
//			stack.pop();
//		} catch (NoSuchElementException e)
//		{
//			System.out.println("Empty stack, you did a no no!");
//		}
//		System.out.println("Stack empty? " + stack.empty());

		/*
		 * The following 1000000 push and pops are really 
		 * slow for the "really" bad lines in StackVector!
		 */
		System.out.println("Begin");
		for (int i=0; i < 1000000; i++)
		{
			stack.push("Yeah");
		}
		
		for (int i=0; i < 1000000 - 1; i++)
		{
			stack.pop();
		}
		System.out.println("End");
		/*
		 * Problem with Java Stack is that it violates the
		 * true meaning of a stack by extending a Vector
		 */
		//stack.add(3, "Many");

	}


}
