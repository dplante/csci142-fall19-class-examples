package structures;

import java.util.Vector;

/**
 * Stack that uses a Vector internally rather than extending
 * a Vector as the Java API does.  This "wrapping" shields 
 * the other functionality of a Vector that should not exist 
 * for a stack.
 * 
 * @author dplante
 *
 * @param <T>
 */
public class StackArray
{
	private final static int EMPTY_INDEX = -1;
	private Object[] myData;
	private int myMaxSize;
	private int myTop;
	
	public StackArray(int num)
	{
		myMaxSize = num;
		myData = new Object[num];
		myTop = EMPTY_INDEX;
	}
	
	/*
	 * Below, we do NOT worry about index out of bounds,
	 * but we should.
	 */
	public void push(Object e)
	{
		myData[++myTop] = e;
	}
	
	public Object pop()
	{
		if (this.empty())
			return null;
		
		return myData[myTop--];
	}
	
	public Object peek()
	{
		return myData[myTop];
	}
	
	public boolean empty()
	{
		return myTop == EMPTY_INDEX;
	}

}
