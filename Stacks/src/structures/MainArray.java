package structures;

public class MainArray 
{

	public static void main(String[] args) 
	{
		StackArray stack = new StackArray(5);
		
		System.out.println("Stack empty? " + stack.empty());

		System.out.println(stack.pop());
		
		stack.push("Tired");
		stack.push("Students");
		stack.push("Love");
		stack.push("Sleeping");
				
		System.out.println("Stack empty? " + stack.empty());
		while (!stack.empty())
		{
			System.out.println(stack.pop());
		}
		System.out.println("Stack empty? " + stack.empty());
	}
}
