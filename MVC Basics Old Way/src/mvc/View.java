package mvc;
/**
 * Simple view class used to show how Model-View-Controller
 * is implemented.  This is also the main application. 
 * Version 2.0 uses Generics.
 *
 * @author Daniel Plante
 * @version 1.0 (28 January 2002)
 * @version 2.0 (1 February 2008)
 */

import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class View implements ActionListener
{
	//////////////////////
    //    Properties    //
    //////////////////////
    
	private JButton myButtonIncrement;
    private JButton myButtonDecrement;
    private JTextField myTextField;
    private Controller myController;
    
    ///////////////////////
    //      Methods      //
    ///////////////////////
    
    /**
     * View constructor used to lay out the view
     *
     * <pre>
     * pre:  none
     * post: the view is set up and initialized
     * </pre>
     */
    public View(Controller controller)
    {
        String value;
        
        myController = controller;
        
		JFrame frame = new JFrame("MVC Basics Old Way");
       
        frame.setSize(300,100);
        frame.setLayout(new FlowLayout());
        
        myButtonIncrement = new JButton("+");
        myButtonDecrement = new JButton("-");
        
        myButtonIncrement.addActionListener(this);
        myButtonDecrement.addActionListener(this);
        
        value = controller.getModelValue();
        myTextField = new JTextField(value);
        
        frame.add(myButtonIncrement);
        frame.add(myButtonDecrement);
        frame.add(myTextField);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Handle events here!
     */
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == myButtonIncrement)
		{
			myController.increment();
		}
		else if (e.getSource() == myButtonDecrement)
		{
			myController.decrement();
		}
	}

    /**
     * Updates myTextField with the String text.
     *
     * @param text the text string to use in updating the text field
     */
    public void setTextField(String text)
    {
        myTextField.setText(text);
    }
}