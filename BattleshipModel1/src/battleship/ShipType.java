package battleship;

public enum ShipType 
{
	CRUISER ("Cruiser"),
	CARRIER ("Carrier"),
	BATTLESHIP ("Battleship"),
	SUBMARINE ("Submarine"),
	DESTROYER ("Destroyer");
	
	private final String myShip;
	
	private ShipType(String ship)
	{
		myShip = ship;
	}
	public String getType()
	{
		return myShip;
	}
}
