package testing;

public class Test2 
{
	/*
	 * Test making myValue private, protected, and public
	 */
	public Test1 myTest1;
	
	public Test2()
	{
		myTest1 = new Test1(1);
	}
	
	/*
	 * Test making myValue private, protected, and public
	 */
	public Test1 method2()
	{
		myTest1.myValue = 5;
		myTest1.method1();
		
		return myTest1;
	}

}
