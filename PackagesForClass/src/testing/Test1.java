package testing;

public class Test1 
{
	/*
	 * Test making myValue private, protected, and public
	 */
	public int myValue;
	
	public Test1(int val)
	{
		myValue = val;
	}
	
	/*
	 * Test making method1 private, protected, and public
	 */
	public void method1()
	{
		myValue++;
		System.out.println("myValue1 = " + myValue);
	}

}
