package math;

/**
 * Solution to the sqrt() program, in case you are interested.
 * 
 * @author dplante
 *
 */
public class SquareRoot {

	public static void main(String[] args) {
		double value = 256;
		System.out.println("Iterative sqrt(" + value + ") = " + sqrt(value));
		System.out.println("Recursive sqrt(" + value + ") = " + sqrtR(value));

	}
	
	public static double sqrt(double x)
	{
		double guess = 1.0;
		double guessSquared = guess * guess;
		
		while (Math.abs(x-guessSquared) >= 0.00000000001) 
		{
			guess += (x-guessSquared)/2.0/guess;
			guessSquared = guess * guess;
		}
		return guess;
	}
	
	public static double sqrtR(double x)
	{
		return sqrtR(x, 1.0);
	}

	private static double sqrtR(double x, double guess)
	{
		double guessSquared = guess * guess;
		
		if ((Math.abs(x-guessSquared) >= 0.00000000001))
		{
			System.out.println(guess + "   " + x);
			guess += (x-guessSquared)/2.0/guess;
			return sqrtR(x, guess);
		}
		return guess;
	}
}
