package booktests;

/**
 * Partial and not very complete set of test cases to illustrate how 
 * test cases can be written, and how they can be used to test code
 * (in this case the code in the Book.java class) even if that code
 * has not been written yet.
 * 
 * @author Daniel Plante 
 */
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import books.Book;

public class BookTestCases {

	private Book myBook;

	/*
	 * Set up a book of 5 pages (this one is on the NY Times Best Seller's 
	 * list for sure!  This book will set up fresh and new for every test case below!
	 */
	@Before
	public void setUp() throws Exception {
		myBook = new Book("I love Dr. Plante's class", 5);
	}

	@Test
	public void testPageNumber() {
		int pageNumber = myBook.getPageNumber();
		
		assertEquals("Book not open, should not be on a page", Book.PAGE_NOT_SET, pageNumber);
	}
	
	@Test
	public void testGoToInvalidPage() {
		boolean validMove = myBook.goToPage(6);
		assertFalse("Cannot go to page 6", validMove);
	}
	
	@Test
	public void testFlipPageWhenBookClosed() {
		boolean validFlip = myBook.flipPage();
		assertFalse("Cannot flip page for closed book", validFlip);
	}
	
	/**
	 * More complex test case where book is opened, page is flipped to a 
	 * valid page, a bookmark is set, the book is closed, the book is then
	 * opened again, and the bookmark from before the book was closed is 
	 * retrieved
	 */
	@Test
	public void bookmarkAndFlip() {
		boolean valid = myBook.open(2);
		valid = valid & myBook.flipPage();
		valid = valid & myBook.setBookmark();
		assertTrue("Book should open to page 2 and flip to page 3", valid);
		assertEquals("Page should be 3", 3, myBook.getPageNumber());
		valid = myBook.close();
		assertTrue("Should be able to close book", valid);
		valid = myBook.open(1);
		assertEquals("Bookmark should be at page 3", 3, myBook.getBookmark());
	}
	
	/**
	 * Flip to last page successfully but try to unsuccessfully flip again
	 */
	@Test
	public void flipPagesTooMany() {
		boolean valid = myBook.open(4);
		valid = valid & myBook.flipPage();
		assertTrue("Should be able to flip to last page", valid);
		valid = myBook.flipPage();
		assertFalse("Should not be able to flip past last page", valid);
	}
	
	/**
	 * Test that bookmarked page is preserved even if flipping and going to
	 * other pages
	 */
	@Test
	public void bookmarkPreserved() {
		boolean valid = myBook.open(2);
		valid = valid & myBook.setBookmark();
		valid = valid & myBook.flipPage();
		valid = valid & myBook.flipPage();
		valid = valid & myBook.goToPage(1);
		assertTrue("Bookmark should be set and we are on valid page", valid);
		assertEquals("Bookmark should be at page 2", 2, myBook.getBookmark());
	}

}
