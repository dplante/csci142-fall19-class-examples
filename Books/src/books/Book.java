package books;

/**
 * Book class that doesn't do much, but is a good illustration of how a 
 * simple book with pages and bookmarks can be "designed" and tested.
 * 
 * @author dplante
 *
 */
public class Book 
{
	/*
	 * Look up difference betweeen "final" and "static"
	 */
	public final static int PAGE_NOT_SET = -1;
	private String myTitle;
	private int myNumberOfPages;
	private int myPageNumber;
	
	/*
	 * Methods
	 */
	/**
	 * Constructo that sets the title and number of pages in the book
	 * 
	 * @param title title of book
	 * @param numPages number of pages in book
	 */
	public Book(String title, int numPages)
	{
		
	}
	
	/**
	 * Open book to a given page number
	 * 
	 * @param pageNumber page number to open book to
	 * @return true if book can be opened to the given page or false if not
	 */
	public boolean open(int pageNumber)
	{
		return true;
	}
	
	/**
	 * Close the book
	 * 
	 * @return true if book can be close or false if book already closed
	 */
	public boolean close()
	{
		return false;
	}
	
	/**
	 * Flip the page assuming the book is already open to a page
	 * 
	 * @return true if page can be flipped, false if not (e.g. if on last page 
	 * of book already, or if the book is closed)
	 */
	public boolean flipPage()
	{
		return false;
	}
	
	/**
	 * Go to a specific page
	 * 
	 * @param page page to go to
	 * @return true if book is open and can go to specific page, false if not successful
	 */
	public boolean goToPage(int page)
	{
		return false;
	}
	
	/**
	 * Go directly to the page where the bookmark is
	 * 
	 * @return true if successful at going to that page or false if not
	 */
	public boolean goToBookmark()
	{
		return false;
	}

	/**
	 * Set bookmark at the page we are at
	 * 
	 * @return true if a valid page, false if not on a valid page
	 */
	public boolean setBookmark()
	{
		return true;
	}
	
	/**
	 * Get page that bookmark is set at
	 * 
	 * @return the page of bookmark or PAGE_NOT_SET if the page is not set
	 */
	public int getBookmark()
	{
		return 0;
	}
	
	/**
	 * Get number of pages in book
	 * 
	 * @return number of pages in book
	 */
	public int getNumberOfPages()
	{
		return 0;
	}
	
	/**
	 * Get the page number the book is open at
	 * 
	 * @return the page number book is open at or PAGE_NOT_SET if the book is 
	 * closed or at an invalid page
	 */
	public int getPageNumber()
	{
		return PAGE_NOT_SET;
	}
	
	/**
	 * Get title of book
	 * 
	 * @return title of book
	 */
	public String getTitle()
	{
		return null;
	}
}
