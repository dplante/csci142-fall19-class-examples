package recursion;

/**
 * Dynamic programming example for solving a recursive problem.
 * 
 * @author dplante
 *
 */
public class FibonacciDyn 
{
	static int[] values;

	public static void main(String[] args) 
	{
		values = new int[51];
		for(int i = 0; i<51 ; i++)
		{
			values[i] = -1;
		}
		System.out.println("Fib(50) = " + fib(50));

	}
	
	public static int fib(int value)
	{
		if (value == 0)
		{
			values[0] = 1;
			//System.out.println(value);
			return 1;
		}
		else if (value == 1)
		{
			values[1] = 1;
			//System.out.println(value);
			return 1;
		}
		else if (value > 1)
		{
			//System.out.println(value);
			if(values[value] != -1)
			{
				return values[value];
			}
			else
			{
				int val = fib(value - 1) + fib(value - 2);
				values[value] = val;
				return val;
			}
		}
		return 0;
	}

}
