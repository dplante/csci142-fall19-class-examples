package recursion;

public class Fibonacci 
{

	public static void main(String[] args) 
	{
		new Fibonacci(50);

	}
	
	public Fibonacci(int num)
	{
		int result = this.fib(num);
		System.out.println("Fib(" + num + ") = " + result);
	}
	
	public int fib(int n)
	{
		if (n == 0 || n == 1)
		{
			return 1;
		}
		else if (n > 1)
		{
			return this.fib(n-1) + this.fib(n-2);
		}
		return 0;
	}

}
