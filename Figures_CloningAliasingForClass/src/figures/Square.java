package figures;
import java.awt.Point;

/**
 * A better Square class
 * 
 * @author dplante
 *
 */
public class Square implements Shape
{
	/*
	 * fields, properties, attributes
	 */
	private float mySideLength;
	private float myArea;
	private Point myUpperLeftPoint;
	
	/*
	 * methods
	 */
	public Square()
	{
		this(1.0f, new Point(1, 1));
	}
	
	public Square(float sideLength, Point point)
	{
		/*
		 * My coding standard is to NOT include the keyword 
		 * "this" for properties, but to include it for methods.
		 */
		this.mySideLength = sideLength;		// don't do this
		myUpperLeftPoint = point;
		this.calculateArea();
	}
	
	private void calculateArea()
	{
		myArea = mySideLength * mySideLength;
	}
	
	public Object clone()
	{
		Square square;
		Point point = new Point(myUpperLeftPoint);
		square = new Square(mySideLength, point);
		return square;
	}
	
	public String toString()
	{
		String string = this.getClass().getName()
				+ ": sideLength = " + this.getSideLength() 
				+ "  area = " + this.getArea()
				+ "  upper left point = " + myUpperLeftPoint;
		return string;
	}

	/*
	 * Setters and Getters
	 * Accessors and Mutators
	 */
	public float getSideLength() 
	{
		return mySideLength;
	}

	public void setSideLength(float sideLength) 
	{
		mySideLength = sideLength;
		this.calculateArea();
	}
	
	public float getArea() 
	{
		return myArea;
	}

	public Point getUpperLeftPoint() 
	{
		return myUpperLeftPoint;
	}

	public void setUpperLeftPoint(Point point) 
	{
		myUpperLeftPoint = point;
	}
	
}
