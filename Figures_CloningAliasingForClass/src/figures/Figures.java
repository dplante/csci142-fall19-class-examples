package figures;
import java.awt.Point;

/**
 * A Figures project with interfaces
 * 
 * @author dplante
 *
 */
public class Figures 
{

	public static void main(String[] args) 
	{
		Square square1, square2;
		Point point = new Point(2,2);
		
		square1 = new Square(3.0f, point);
		//square2 = new Square();
		
		/*
		 * 1. First run the code as is
		 * 2. Then comment the first line below and uncomment the second.
		 *    What happens?  Why?
		 * 3. Why do we need to cast the square1 below?
		 * 4. Note the results when running the code at this point.
		 *    Now uncomment the square1.setUpperLeftPoint(point) line
		 *    below.  Do the results change?  Why?
		 * 5. What change should be made in the Square class to make
		 *    sure this doesn't happen? (Hint:  look in the second
		 *    constructor method.)
		 */
		square2 = square1;
		//square2 = (Square)square1.clone();
		
		square2.setSideLength(5.0f);
		
		point.x = 11;
		point.y = 12;
		//square1.setUpperLeftPoint(point);
		
		System.out.println(square1);
		System.out.println(square2);

	}

}
