package figures;
/**
 * Shape "interface", which differs from an "abstract
 * class" in that ALL methods are abstract, and there
 * are no properties.  This is officially called a
 * "design by contract".
 * 
 * @author dplante
 *
 */
public interface Shape 
{
	/*
	 * By convention, you do not include the "abstract"
	 * keyword with the method stubs, because all of the
	 * methods are abstract by definition.  (Can include
	 * it, but don't)
	 */
	public Object clone();
	public abstract String toString();
	public float getArea();
}
