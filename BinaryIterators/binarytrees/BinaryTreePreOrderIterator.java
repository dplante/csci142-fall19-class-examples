package binarytrees;
/**
 * Class:    BinaryTreePreOrderIterator
 * Purpose:
 * 
 * Date:     Apr 14, 2004
 * @author   dplante
 * @version  1.0
 *
 */

import java.util.Vector;

public class BinaryTreePreOrderIterator extends BinaryTreeIterator
{
	public BinaryTreePreOrderIterator(BinaryTree tree)
	{
		super(tree);
	}
	
	public void buildTreeVector(BinaryTree tree)
	{
	   Object datum;
	   Vector vector;
	   
	   vector = this.getTreeVector();
	   if(tree != null)
	   {
		  datum = tree.getData();
		  vector.addElement(datum);
		  this.buildTreeVector(tree.getLeftTree());
		  this.buildTreeVector(tree.getRightTree());
	   }
	}

}
