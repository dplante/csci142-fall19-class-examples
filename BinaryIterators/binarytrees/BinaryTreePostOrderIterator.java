package binarytrees;
/**
 * Class:    BinaryTreePostOrderIterator
 * Purpose:
 * 
 * Date:     Apr 14, 2004
 * @author   dplante
 * @version  1.0
 *
 */

import java.util.Vector;

public class BinaryTreePostOrderIterator extends BinaryTreeIterator
{
	public BinaryTreePostOrderIterator(BinaryTree tree)
	{
		super(tree);
	}
	
	public void buildTreeVector(BinaryTree tree)
	{
	   Object datum;
	   Vector vector;
	   
	   vector = this.getTreeVector();
	   if(tree != null)
	   {
		  datum = tree.getData();
		  this.buildTreeVector(tree.getLeftTree());
		  this.buildTreeVector(tree.getRightTree());
		  vector.addElement(datum);
	   }
	}

}
