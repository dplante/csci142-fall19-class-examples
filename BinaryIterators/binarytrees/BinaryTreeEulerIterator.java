package binarytrees;
/**
 * Class:    BinaryTreeEulerIterator
 * Purpose:
 * 
 * Date:     Apr 14, 2004
 * @author   dplante
 * @version  1.0
 *
 */
import java.util.Vector;

public class BinaryTreeEulerIterator extends BinaryTreeIterator
{
	public BinaryTreeEulerIterator(BinaryTree tree)
	{
		super(tree);
	}
	
	public void buildTreeVector(BinaryTree tree)
	{
	   Object datum;
	   Vector vector;
	   
	   vector = this.getTreeVector();
	   if(tree != null)
	   {
		  datum = tree.getData();
		  if(tree.hasLeftTree() && tree.hasRightTree())
		  {
		  	vector.addElement("(");
		  }
		  this.buildTreeVector(tree.getLeftTree());
		  vector.addElement(datum);
		  this.buildTreeVector(tree.getRightTree());
		  if(tree.hasLeftTree() && tree.hasRightTree())
		  {
		    vector.addElement(")");
		  }

	   }
	}

}