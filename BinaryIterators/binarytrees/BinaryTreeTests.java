package binarytrees;
/**
 * Class:  BinaryTreeTests
 * @author Daniel Plante
 * @version 1.0
 */

import java.util.Iterator;

public class BinaryTreeTests
{
    public static void main(String[] args)
    {
        new BinaryTreeTests();
    }
    
    public BinaryTreeTests()
    {
    	this.binaryExpressionTest();
    	this.binarySearchTest();
    }
    
    public void binarySearchTest()
    {
    	BinarySearchTree tree;
    	
    	tree = new BinarySearchTree();
    	
		tree.insert(new Integer(6));
		tree.insert(new Integer(46));
		tree.insert(new Integer(9));
		tree.insert(new Integer(-16));
		tree.insert(new Integer(6));
		tree.insert(new Integer(3));
		
		Object object;
		String str, expression;

		Iterator it;
		it = tree.iterator();

		// In order is the default iterator for a BinarySearchTree;
		// BinaryExpressionTrees can have either pre-, post- ore in-order
		// iterators.
		expression = "";
		while(it.hasNext())
		{
			object = it.next();
			str = " " + object.toString() + " ";
			expression += str;
		}
		System.out.println("In Order:" + expression + "\n");

    }
    
    public void binaryExpressionTest()
    {
        BinaryExpressionTree tree, leftTree, rightTree;
        
        leftTree = new BinaryExpressionTree("15", null, null);
        rightTree = new BinaryExpressionTree("3", null, null);
        rightTree = new BinaryExpressionTree("/", leftTree, rightTree);
        leftTree = new BinaryExpressionTree("8", null, null);
        leftTree = new BinaryExpressionTree("+", leftTree, rightTree);
        rightTree = new BinaryExpressionTree("4", null, null);
        tree = new BinaryExpressionTree("-", leftTree, rightTree);
        
        /* Print Expression Tree using Iterators */
        Iterator it;
		Object object;
		String str, expression;
		
		// Pre order
		expression = "";
		it = new BinaryTreePreOrderIterator(tree);
		
		while(it.hasNext())
		{
			object = it.next();
			str = " " + object.toString() + " ";
			expression += str;
		}
		System.out.println("Pre Order:" + expression + "\n");

		// Post order
		expression = "";
		it = new BinaryTreePostOrderIterator(tree);

		while(it.hasNext())
		{
			object = it.next();
			str = " " + object.toString() + " ";
			expression += str;
		}
		System.out.println("Post Order:" + expression + "\n");

		// In order
		expression = "";
		it = new BinaryTreeInOrderIterator(tree);

		while(it.hasNext())
		{
			object = it.next();
			str = " " + object.toString() + " ";
			expression += str;
		}
		System.out.println("In Order:" + expression + "\n");

		// Euler in order
		expression = "";
		it = new BinaryTreeEulerIterator(tree);

		while(it.hasNext())
		{
			object = it.next();
			str = " " + object.toString() + " ";
			expression += str;
		}
		System.out.println("Euler In Order:" + expression + "\n");
    }
}