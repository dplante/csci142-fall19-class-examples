package binarytrees;


/**
 * Class:    BinaryExpressionTree
 * Purpose:
 * 
 * Date:     Apr 14, 2004
 * @author   dplante
 * @version  1.0
 *
 */
public class BinaryExpressionTree extends BinaryTree
{
	//////////////////////////////////////////////
	//            Properties                    //
    //////////////////////////////////////////////
    
	//////////////////////////////////////////////
	//            Methods                       //
	//////////////////////////////////////////////
	public BinaryExpressionTree()
	{
		super();
		this.setData(null);
	}
  
	public BinaryExpressionTree(Object datum, BinaryExpressionTree left, 
	                                              BinaryExpressionTree right)
	{
	   this.setParent(null);
	   this.setData(datum);
	   this.setLeftTree(left);
	   this.setRightTree(right);
	}

	// Must implement
	public boolean isOperand(BinaryExpressionTree tree)
	{
		return true;
	}
	
	public boolean isOperator(BinaryExpressionTree tree)
	{
		return true;
	}
}
