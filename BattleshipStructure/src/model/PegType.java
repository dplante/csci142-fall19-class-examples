package model;

public enum PegType 
{
	RED ("Red"),
	WHITE ("White"),
	NONE ("None");
	
	private final String myType;
	
	private PegType(String type)
	{
		myType = type;
	}
	
	public String getType()
	{
		return myType;
	}

}
