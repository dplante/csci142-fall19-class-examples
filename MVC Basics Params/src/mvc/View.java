package mvc;
/**
 * Simple view class used to show how Model-View-Controller
 * is implemented.  This is also the main application.
 * Version 2.0 uses Generics.
 *
 * @author Daniel Plante
 * @version 1.0 (28 January 2002)
 * @version 2.0 (1 February 2008)
 */

import java.awt.*;
import java.lang.reflect.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import java.lang.Integer;

@SuppressWarnings("serial")
public class View
{
    //////////////////////
    //    Properties    //
    //////////////////////
    
	private JButton myButtonIncrement;
    private JButton myButtonDecrement;
    private ButtonListener myIncrementListener;
    private ButtonListener myDecrementListener;
    private JTextField myTextField;
    private Controller myController;
    
    ///////////////////////
    //      Methods      //
    ///////////////////////
        
    /**
     * View constructor used to lay out the view
     *
     * <pre>
     * pre:  none
     * post: the view is set up and initialized
     * </pre>
     */
    public View(Controller controller)
    {
        String value;
        
        JFrame frame = new JFrame("MVC Basics with Parameters");
        
        frame.setSize(300,100);
        frame.setLayout(new FlowLayout());
        
        myController = controller;
        
        this.associateListeners();
        
        myButtonIncrement = new JButton("+");
        myButtonDecrement = new JButton("-");
        
        myButtonIncrement.addMouseListener(myIncrementListener);
        myButtonDecrement.addMouseListener(myDecrementListener);
        
        value = myController.getModelValue();
        myTextField = new JTextField(value);
        
        frame.add(myButtonIncrement);
        frame.add(myButtonDecrement);
        frame.add(myTextField);
        
        frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     * Associates each component's listener with the controller
     * and the correct method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    public void associateListeners()
    {
        String error;
        Class<? extends Controller> controllerClass;
        Method incrementMethod,
               decrementMethod;
        Class<?>[] classArgs;
        Integer[] args;
        
        controllerClass = myController.getClass();
        
        error = null;
        incrementMethod = null;
        decrementMethod = null;

        classArgs = new Class[1];
        args = new Integer[1];
        
        // Set argument types for method invokations
        try
        {
           classArgs[0] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException exception)
        {
           error = exception.toString();
           System.out.println(error);
        }
        
        // Associate method names with actual methods to be invoked
        try
        {
           incrementMethod = controllerClass.getMethod("increment",classArgs);
           decrementMethod = controllerClass.getMethod("decrement",classArgs);        
        }
        catch(NoSuchMethodException exception)
        {
           error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           error = exception.toString();
           System.out.println(error);
        }
        
        // Set up listeners with actual argument values passed in
        // to methods
        
        args[0] = new Integer(20);
        myIncrementListener 
               = new ButtonListener(myController, incrementMethod, args);
        myDecrementListener 
               = new ButtonListener(myController, decrementMethod, args);
    }
    
    /**
     * Updates myTextField with the String text.
     *
     * @param text the text string to use in updating the text field
     */
    public void setTextField(String text)
    {
        myTextField.setText(text);
    }
    
}