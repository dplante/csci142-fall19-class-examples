package mvc;
/**
 * Simple controller class used to show how Model-View-Controller
 * is implemented.
 *
 * @author Daniel Plante
 * @version 1.0 (28 January 2002)
 */
public class Controller
{
    ///////////////////
    //  Properties   //
    ///////////////////
     
    private View myView;
    private Model myModel;
    
    ///////////////////
    //    Methods    //
    ///////////////////
    
    /**
     * Default controller constructor.
     */
    public Controller()
    {
        /*
         * view is instantiated right
         * within this class rather than being passed in. Order is 
         * important, so myModel must be instantiated before myView.
         */
        myModel = new Model();
        myView = new View(this);
    }
    
    /**
     * Controller constructor; view must be passed in since 
     * controller has responsibility to notify view when 
     * some event takes place.
     *
     * @param view the View this controller has a responsibility to 
     */
    public Controller(View view)
    {
        myView = view;
        myModel = new Model();
    }
    
    /**
     * Modifies the number value of the model and gives it to
     * the view.
     *
     * <pre>
     * pre:  a valid view and controller have been designated
     * post: the number value of the model is increased by one,
     *       and the view is modified accordingly
     * </pre>
     *
     * @param amount amount to increase text value by
     */
    public void increment(Integer amount)
    {
        int value;
        myModel.incrementValue(amount.intValue());
        value = myModel.getNumberValue();
        myView.setTextField(""+value);
    }
    
    /**
     * Modifies the number value of the model and gives it to
     * the view.
     *
     * <pre>
     * pre:  a valid view and controller have been designated
     * post: the number value of the model is decreased by one,
     *       and the view is modified accordingly
     * </pre>
     *
     * @param amount amount to decrease text value by
     */
    public void decrement(Integer amount)
    {
        int value;
        myModel.decrementValue(amount.intValue());
        value = myModel.getNumberValue();
        myView.setTextField(""+value);
    }
    
    /**
     * Obtains the number value of the model.
     *
     * <pre>
     * pre:  a valid view and controller have been designated
     * post: the number value of the model obtained
     * </pre>
     *
     * @return the model's number value
     */
    public String getModelValue()
    {
        String modelValue;
        
        modelValue = ""+myModel.getNumberValue();
        return modelValue;
    }
    
}